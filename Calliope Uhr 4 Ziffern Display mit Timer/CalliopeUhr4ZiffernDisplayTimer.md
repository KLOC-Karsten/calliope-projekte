# Calliope Uhr mit 4-Ziffern Display und Timer-Funktion

Dieses Beispiel ist für die **Calliope v3** entwickelt.

## Hardware und Aufbau

### Hardwareliste

Die folgende Hardware wird benötigt:

- [1x Calliope mini 3](https://calliope.cc/) mit Stromversorgung (via USB, Batterypack, etc)
- [Grove 4 Digit Display](https://www.seeedstudio.com/Grove-4-Digit-Display.html?queryID=940b601be28c71904c7d2c1e492cb1dc&objectID=1651&indexName=bazaar_retailer_products) 
- [1x Grove Dual Button](https://www.seeedstudio.com/Grove-Dual-Button-p-4529.html?queryID=b5950c6644bf196773ae054242789663&objectID=4529&indexName=bazaar_retailer_products)
- [1x Grove 4 Pin Male Jumper to Grove 4-Pin Conversion Cable](https://www.seeedstudio.com/Grove-4-pin-Male-Jumper-to-Grove-4-pin-Conversion-Cable-5-PCs-per-Pack.html?queryID=de1a6896909b64f8b32e124d061da918&objectID=1321&indexName=bazaar_retailer_products) 
- [2x Grove Wrapper 1x2](https://www.seeedstudio.com/Grove-Yellow-Wrapper-1-2-4-PCS-pack.html?queryID=32e831e4edcd1b42f0374b09bfbfd9fd&objectID=564&indexName=bazaar_retailer_products)
- eine Lego Basisplatte, ca 13 x 7 cm. 
- Gummiband, um das Calliope mini Board an der Lego Basisplatte zu fixieren. 
- ein paar Lego Klötzchen oder Draht, um die Grove Kabel zu fixieren
- ein Aufsteller, entweder aus Pappe gebastelt oder mit Lego Klötzchen gebaut. 

### Aufbau 

![Aufbau](Bilder/Uhr-Timer.jpg "Aufbau")

1. Das 4 Ziffern Display mit dem beiliegenden Grove-Kabel verbinden und in den Grove Wrapper drücken. 
  Den Wrapper dann auf der Lego Basisplatte befestigen. 

2. Das andere Ende des Grove Kabels in die A1-Grove-Buchse der Calliope mini stecken. 

3. Den Grove-Connector des Grove 4-Pin Male Jumper auf den Grove Dual-Button stecken, dann den Dual-Button in den zweiten 
   Grove Wrapper drücken und den Wrapper auf der Lego Basisplatte befestigen.

4. Die 4 Pins des Grove-Kabels wie folgt mit der Pin-Leiste des Calliope minis verbinden 
   (die Calliope Pin Belegung ist [hier](https://calliope.cc/calliope-mini/technische-daten) dokumentiert, 
    die Dokumentation der Grove Anschlüsse befinden sich [hier](https://wiki.seeedstudio.com/Grove_System/#-more-knowledge-about-grove-ecosystm-)):
   - das schwarze Kabel links unten mit "GND" verbinden.
   - das rote Kabel links oben mit "VCC" verbinden
   - das weiße Kabel mit "P0" verbinden (rechts neben den schwarzen Kabel)
   - das gelbe Kabel mit "P1" verbinden (rechts neben den roten Kabel)

5. Mit dem Gummiband und ein paar Lego-Klötzchen die Calliope mini auf der Basisplatte befestigen, 
   mit Lego oder anderen Hilfsmitteln eine Kabelführung basteln. 

6. Das weiter unten beschriebene Programm laden, mit einer Stromquelle verbinden und auf einen Aufsteller setzen. 

## Das Programm

Das Programm wurde mit makecode entwickelt und kann von [github](https://github.com/KLOC-Karsten/calliope_uhr_v2) heruntergeladen 
werden. 

### Wie es funktionieren soll

Die Uhr hat zwei Funktionen: die Zeit anzeigen und ein einfachen Countdown Timer, dessen Laufzeit eingestellt werden kann, 
und der nach Ablauf eine kleine Melodie abspielt. 


Die Uhr ist nicht gepuffert und es wird kein Echtzeit-Modul benutzt, die Uhr muss also jedesmal neu eingestellt werden, wenn 
das Calliope Board mit Spannung versorgt wird. 

Um die Uhrzeit einzustellen, muss zunächst der Knopf "A" auf der Calliope mini gedrückt werden, es erscheint ein kleines Bild 
auf dem LED Display und dann kann mit den beiden Grove Buttons die Uhrzeit eingestellt werden. 
Mit dem Aufbau vom Bild oben ist der rechte blaue Button (neben dem Grove Stecker) für die Minuten und der linke grüne Button 
für die Stunden zuständig. Wird der Knopf "A" ein weiteres Mal gedrückt, dann ist die Uhrzeit eingestellt und das Bild
auf der LED Matrix verschwindet.

Um den Timer einzustellen, muss als Knopf "B" gedrückt werden (auf der rechten Seite des Calliope Boards), es erscheint wieder ein
Symbol auf der LED Matrix, dann werden mit den beiden Grove Buttons die Minuten und Sekunden eingestellt. Wird jetzt Knopf "B" 
gedrückt, wird der Timer gestartet. Der Timer läuft, die Restzeit wird angezeigt (in Minuten und Sekunden), und wenn die 
Zeit abgelaufen ist, dann wird eine kleine Melodie abgespielt. Danach wird wieder die Uhrzeit angezeigt.   


### Initialisierung

Am Anfang werden die benutzten Variablen initialisiert. 
Dies sind:

- die Variablen für die Uhrzeit (millisekunden, sekunden, minuten, stunden, startzeit_uhr)
- die Variablen für den Timer (laufzeit_timer)
- Variablen für den Einstellmodus (uhr_wird_gestellt, timer_wird_gestellt, timer_laeuft)
- Variablen für die Anzeige (anzeige, ziffern_alt)

![Beim Start](Bilder/beim_start.png "Beim Start")

### Die Hauptschleife
Die Hauptschleife ist relativ einfach und orientiert sich am aktuellen Modus:

- wird gerade der Timer eingestellt, dann wird der Timerwert angezeigt
- läuft der Timer gerade, dann wird die Restzeit des Timers angezeigt (Countdown), 
- ansonsten (die Uhr wird gestellt oder normaler Betrieb) wird die Uhrzeit angezeigt

![Dauerhaft](Bilder/dauerhaft.png "Dauerhaft")

### Ereignisse

Das Programm reagiert auf vier Ereignisse: entweder wird einer der beiden eingebauten Knöpfe gedrückt, 
oder einer der an P0 und P1 angeschlossenen Knöpfe. 


Wenn Knopf A gedrückt wird, und die Uhr wird gerade nicht gestellt, dann wird in den Modus "Uhrzeit wird gestellt" gewechselt. 
Dies wird auf durch eine Grapfik am Display dargestellt. 

Wenn Knopf A gedrückt wird, und die Uhr wird gerade gestellt, dann wird der Modus "Uhrzeit wird gestellt" verlassen
und das Display wird zurückgesetzt. 

![Knopf A wird gedrückt](Bilder/wenn_knopf_A_geklickt.png "Knopf A")


Wenn Knopf B gedrückt wird, und der Timer wird gerade nicht gestellt, dann wird in den Modus "Timer wird gestellt" gewechselt. 
Dies wird auf durch eine Grapfik am Display dargestellt. 

Wenn Knopf B gedrückt wird, und der Timer wurde gestellt, dann wird in den Modus "Timer läuft" gewechselt.
Dies wird auf durch eine Grapfik am Display dargestellt.
*Wichtig*: die Startzeit des Timers (als Laufzeit in ms) wird jetzt gespeichert. 

![Knopf B wird gedrückt](Bilder/wenn_knopf_B_geklickt.png "Knopf B")

Wenn Pin P0 gedrückt wird, und es wird gerade die Uhrzeit eingestellt, dann wird die Startzeit um 60 Sekunden erhöht. 
Dadurch wird die aktuelle Uhrzeit um eine Minute erhöht.

Wenn Pin P0 gedrückt wird, und es wird gerade der Timer eingestellt, dann wird die Laufzeit des Timers um eine Sekunde erhöht. 

![P0 wird gedrückt](Bilder/wenn_pin_P0_gedrueckt.png "Pin P0")

Wenn Pin P1 gedrückt wird, und es wird gerade die Uhrzeit eingestellt, dann wird die Startzeit um 3600 Sekunden erhöht. 
Dadurch wird die aktuelle Uhrzeit um eine Stunde erhöht.

Wenn Pin P0 gedrückt wird, und es wird gerade der Timer eingestellt, dann wird die Laufzeit des Timers um eine Minute erhöht. 

![P1 wird gedrückt](Bilder/wenn_pin_P1_gedrueckt.png "Pin P1")


### Uhrzeit aktualisieren

Die Uhrzeit wird wie folgt berechnet: 
- die aktuelle Laufzeit in Millisekunden wird gelesen und in Sekunden umgerechnet. Die Startzeit wird hinzuaddiert. 
  Dies ergibt die Zeit in Sekunden. 
- Daraus werden dann die Stunden und Minuten abgeleiete. Am Ende ist "minuten" ein Wert zwischen 0 und 59 und "Stunden" ein 
  Wert zwischen 0 und 23. 

![Uhrzeit aktualisieren](Bilder/aktualisiere_uhrzeit.png "Uhrzeit aktualisieren")


### Restzeit des Timers aktualisieren

Die Restlaufzeit des Timers wird wie folgt berechnet. 

- Als erstes wird die Differenz zwischen der aktuellen Laufzeit und der  Startzeit des Timers 
  berechnet und in Sekunden umgerechnet. Das ist die bislang verstrichende Zeit seit dem Start des Timers.
- Die Restzeit ist dann die Differenz zwischen der Soll-Laufzeit des Timers und der schon verstrichende Zeit. 
- Ist die Differenz kleiner als oder gleich Null, dann ist der Timer abgelaufen, 
  die Restzeit 0 wird angezeigt und eine kline Melodie wird abgespielt. Ausserdem wird das LED Display zurückgesetzt. 

![Timer-Restzeit aktualisieren](Bilder/aktualisiere_restzeit.png "Restzeit aktualisieren")

### 4 Ziffern Display 

Alle Zeiten werden als 4-stellige Zahl am Display dargestellt. 
Für die Uhrzeit werden dafür die Stunden mit 100 multpliziert und die Minuten hinzuaddiert. 

*Beispiel*: aus der Uhrzeit "15 Uhr 39" wird die Zahl 1539.

![Uhrzeit anzeigen](Bilder/zeige_uhrzeit.png "Uhrzeit anzeigen")

Für die Uhrzeit werden dafür die Stunden mit 100 multpliziert und die Minuten hinzuaddiert. 

![Uhrzeit anzeigen](Bilder/zeige_uhrzeit.png "Uhrzeit anzeigen")

Für die eingestellte Timer-Laufzeit wird dasselbe gemacht. Nur müssen dazu aus der "laufzeit_timer" Variable die Sekunden
und Minuten getrennt werden. 
Beispiel: sind 210 Sekunden als Laufzeit für den Timer eingestellt, dann ergibt das 2 Minuten und 30 Sekunden 
und es wird der Wert 230 berechnet. Am  Display wird dann "02:30" dargestellt. 

![Laufzeit anzeigen](Bilder/zeige_timer.png "Laufzeit anzeigen")

Das selbe Verfahren noch einmal für die Restzeit.

![Restzeit anzeigen](Bilder/zeige_restzeit.png "Restzeit anzeigen")

Die Funktion "zeige_wert" zeigt dann die Zahl an. Dafür wird jede Ziffer einzeln berechnet und mit der aktuell angezeigten 
Ziffer verglichen. Nur wenn eine andere Ziffer dargestellt werden muss, wird das Display an der Stelle aktualisiert. 
Was am Display erscheint, wird also in der Array-Variablen "ziffern_alt" gespeichert. 


*Beispiel*: um die dritte Ziffer von rechts ("hunderte") zu berechnen, wird der Wert (z.B. 1539) durch 100  geteilt und abgerundet
(ergibt 15). Dann wird der Rest von der Division mit 10 berechnet (ergibt 5). 
Die erste Ziffer von Links hat Index 0 und die zweite hat Index 1, also wird überprüft, ob die Ziffer mit Index 1 aktualisiert werden muss. 

![Werte anzeigen](Bilder/zeige_wert.png "Werte anzeigen")




