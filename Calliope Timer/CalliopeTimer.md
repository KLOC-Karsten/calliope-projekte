# Calliope Timer

Ein einfaches Timer Programm für die Calliope Mini. 
Initial ist die Dauer vom Timer 5 Minuten. 
Wenn der Timer noch nicht gestartet worden ist, dann kann mit Taste A die Dauer um jeweils 5 Minuten erhöht werden. 
Mit Taste B wird dann der Timer gestartet. 

Während der Timer läuft wird jede Minute die Restdauer angezeigt. Am Ende gibt es ein Bildchen, die LED leuchtet 
und es werden 3 Töne gespielt. 

## Hauptprogramm

Das Hauptprogramm überprúft, ob der Timer schon gestartet worden ist. 
Wenn nicht, dann kann die Dauer geändert werden (Taste A) oder es wird der Timer gestartet (Taste B) oder
es wird nichts getan. 

Wenn der Timer läuft, wird überprüft, ob die Zeit abgelaufen ist, oder ob es Zeit ist, die Restdauer anzuzeigen. 

![Hauptprogramm](Bilder/calliopeTimer.png "Hauptprogramm")

## Eingestellte Timer Dauer anzeigen

Eine einfache Funktion, die die eingestellte Dauer in Minuten anzeigt. 

![Zeige Dauer](Bilder/zeigeDauer.png "Zeige Dauer")

## Restdauer anzeigen

Eine einfache Funktion, die die Restdauer in Minuten anzeigt (wenn der Timer läuft)

![Zeige Restdauer](Bilder/zeigeRestdauer.png "Zeige Restdauer")

