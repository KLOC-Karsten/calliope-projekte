# Calliope Projekte


Eine Reihe von einfachen Projekten für den Calliope Mini Rechner. 

## Projekte nach Calliope Version

Bis auf die folgenden Beispiele, sind alle Projekte für Calliope V2 entwickelt worden

### Projekte für die neue Calliope Version v3
- [Calliope Uhr 4 Ziffern Display und Timer Funktion](Calliope Uhr 4 Ziffern Display mit Timer/CalliopeUhr4ZiffernDisplayTimer.md) - 
  eine einfache, einstellbare Uhr. Die Darstellung der Uhrzeit erfolgt hier auf einen 4 Ziffern Display. Es kann auch ein Timer eingestellt werden. 


## Projekte nach Thema
### Projekte für das Thema "Zeit"

- [Calliope Uhr](Calliope Uhr/CalliopeUhr.md) - eine einfache, einstellbare Uhr. 
- [Calliope Uhr 4 Ziffern Display](Calliope Uhr 4 Ziffern Display/CalliopeUhr4ZiffernDisplay.md) - eine einfache, einstellbare Uhr. Die Darstellung der Uhrzeit erfolgt hier auf einen 4 Ziffern Display.
- [Calliope Timer](Calliope Timer/CalliopeTimer.md) - ein einfacher Timer. 
- [Calliope Uhr 4 Ziffern Display und Timer Funktion](Calliope Uhr 4 Ziffern Display mit Timer/CalliopeUhr4ZiffernDisplayTimer.md) - 
  eine einfache, einstellbare Uhr. Die Darstellung der Uhrzeit erfolgt hier auf einen 4 Ziffern Display. Es kann auch ein Timer eingestellt werden. 
  Dieses Beispiel ist für die Calliope Version V3 gedacht. 




### Projekte zum Thema "Luft" / "Klima"

Programme, die mit [MakeCode](https://makecode.calliope.cc) für Calliope Mini entstanden sind. 

- [Calliope Luftqualität](Calliope Luftqualität/CalliopeLuftqualitaet.md) - benutzt den Air Quality Sensor um die Luftqualität anzuzeigen.
- [CO2 Messung](Calliope CO2 Sensor/CalliopeCO2Sensor.md) - misst die Temperatur, Feuchtigkeit und den CO2 Gehalt der Luft.
- [Temperatur](Calliope Temperatur/CalliopeTemperatur.md) - verschiedene Beispiele mit Temperatursensoren.


### Projekte zum Thema "Farbe"

- [Farbsensor](Calliope Farbsensor/CalliopeFarbsensor.md) - Beispiele mti dem Grove Farbsensor.

### Projekte mit Displays

- [Uhr mit 4 Ziffern Display](Calliope Uhr 4 Ziffern Display/CalliopeUhr4ZiffernDisplay.md) - eine einfache, einstellbare Uhr. 
- [Temperatur am LCD ausgeben](Calliope Temperatur/CalliopeTemperatur.md) - gibt Temperaturinformationen auf einen Grove LCD Display aus. 
- [Farbsensor mit Ausgabe](Calliope Farbsensor/CalliopeFarbsensor.md) - Ein Farbsensor wird benutzt, um Farbe zu erkennen; 
die Farbwerte werden dann auf einen LCD Display ausgegeben.


## Projekte nach Entwicklungsumgebung

### Open Roberta Lab Programme

Diese Programme sind mit [Open Roberta Lab](https://lab.open-roberta.org) entstanden.

- [Calliope Uhr](Calliope Uhr/CalliopeUhr.md) - eine einfache, einstellbare Uhr. 
- [Calliope Uhr 4 Ziffern Display](Calliope Uhr 4 Ziffern Display/CalliopeUhr4ZiffernDisplay.md) - eine einfache, einstellbare Uhr. 
  Die Darstellung der Uhrzeit erfolgt hier auf einen 4 Ziffern Display.
- [Calliope Timer](Calliope Timer/CalliopeTimer.md) - ein einfacher Timer. 
- [Calliope Luftqualität](Calliope Luftqualität/CalliopeLuftqualitaet.md) - benutzt den Air Quality Sensor um die Luftqualität anzuzeigen.


### MakeCode Programme

- [CO2 Messung](Calliope CO2 Sensor/CalliopeCO2Sensor.md) - misst die Temperatur, Feuchtigkeit und den CO2 Gehalt der Luft.
- [Temperatur](Calliope Temperatur/CalliopeTemperatur.md) - verschiedene Beispiele mit Temperatursensoren.
- [Farbsensor](Calliope Farbsensor/CalliopeFarbsensor.md) - Beispiele mit dem Grove Farbsensor.
- [Calliope Uhr 4 Ziffern Display und Timer Funktion](Calliope Uhr 4 Ziffern Display mit Timer/CalliopeUhr4ZiffernDisplayTimer.md) - 
  eine einfache, einstellbare Uhr. Die Darstellung der Uhrzeit erfolgt hier auf einen 4 Ziffern Display. Es kann auch ein Timer eingestellt werden. 
  Dieses Beispiel ist für die Calliope Version V3 gedacht. 


