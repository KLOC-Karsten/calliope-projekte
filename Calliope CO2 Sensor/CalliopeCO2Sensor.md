
# Calliope CO2 Sensor

Die Beispiele hier benutzen alle den SCD30 Sensor von Sensirion, verbaut von SeedStudio mit Grove Interface. 
Der Sensor muss am A0 Stecker des Calliope Minis angeschlossen werden. 

Das [Umweltbundesamt](https://www.umweltbundesamt.de/themen/gesundheit/kommissionen-arbeitsgruppen/ausschuss-fuer-innenraumrichtwerte#hygienische-leitwerte)
hat folgende Leitwerte ermittelt
- Eine CO2 Konzentration unter 1000 ppm gilt als unbedenklich
- Eine CO2 Konzentration zwischen 1000 und 2000 ppm ist bedenklich. Also Fenster auf und lüften!
- Werte von mehr als 2000 ppm sind inakzeptabel. 

## Technische Informationen

- [Calliope Webseite für den Sensor](https://calliope.cc/calliope-mini/erweiterungen/sensoren/co2-sensor)
- [MakeCode Erweiterung](https://github.com/calliope-mini/pxt-SCD30)
- [Schnittstellen Beschreibung von Sensirion](https://sensirion.com/media/documents/D7CEEF4A/6165372F/Sensirion_CO2_Sensors_SCD30_Interface_Description.pdf)

## Aufbau

- Anschluss: **A0**

## Implementierung mit MakeCode und SCD30 Erweiterung

Diese Implementierung nutzt die Erweiterung und gibt einfach die gemessenen Werte aus. 

Das MakeCode Projekt befindet sich auf [GitHub](https://github.com/KLOC-Karsten/co2-mit-erweiterung)

![CO2 Messung, mit Erweiterung](MakeCodeErweiterung/CO2-MakeCode-Erweiterung.png "CO2 Messung mit Erweiterung")


## Implementierung mit MakeCode und Python

Funktional identisch, nur eine reine MakeCode/Python Implementierung ohne die Erweiterung. 

Das MakeCode Projekt befindet sich auf [GitHub](https://github.com/KLOC-Karsten/co2-sensor-calliope-mini)



