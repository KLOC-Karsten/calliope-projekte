# Calliope Uhr

Ein einfaches Uhr Programm für die Calliope Mini. 
Zeigt alle 10 Sekunden die Uhrzeit an, 
durch Button A wird der Stundenzähler erhöht, 
durch Button B wird der Minutenzähler erhöht. 

## Hauptprogramm

Das Hauptprogramm startet einen Timer und überprúft dann laufend, 
ob Taste A oder B gedrückt worden sind. 
Falls ja, werden die Stunden oder Minuten erhöht (eingestellt), 
falls nein, wird die Uhrzeit aktualisiert, und alle 10 Sekunden angezeigt. 


![Hauptprogramm](Bilder/hauptProgramm.png "Hauptprogramm")

## Uhrzeit aktualisieren

Der Wert des internen Zeitgebers wird in Sekunden umgerechnet und die Startzeit wird dazu addiert. 
Dann wird aus den Sekunden die aktuelle Uhrzeit in Stunden, Minuten und Sekunden berechnet. 
Alle 24 Stunden wird der Zeitgeber zurückgesetzt. 

![Aktualisiere Uhrzeit](Bilder/aktualisiereUhrzeit.png "Aktualisiere Uhrzeit")

## Uhrzeit anzeigen

Die Uhrzeit Werte werden in einen Text umgewandelt und durch die Calliope LED Matrix angezeigt. 

![Zeige Uhrzeit](Bilder/zeigeUhrzeit.png "Zeige Uhrzeit")

## Stunden einstellen

Diese Funktion wird ausgerufen, wenn Button A gedrückt wird. Die Startzeit wird um eine Stunde (3600 Sekunden) erhöht. 
Danach wird die Uhrzeit neu berechnet und ausgegeben. 

![erhoeheStunden](Bilder/erhoeheStunden.png "erhoeheStunden")

## Minuten einstellen

Diese Funktion wird ausgerufen, wenn Button B gedrückt wird. Die Startzeit wird um eine Minute (60 Sekunden) erhöht. 
Danach wird die Uhrzeit neu berechnet und ausgegeben. 


![erhoeheMinuten](Bilder/erhoeheMinuten.png "erhoeheMinuten")

