# Calliope Farbsensor


## Informationen zum Sensor

Angeschlossen wird der Sensor am Grove Anschluss A0.

- [Informationen auf der Calliope Webseite](https://calliope.cc/calliope-mini/erweiterungen/sensoren/tcs3414-farbsensor)

- [Informationen von Seeed](https://seeeddoc.github.io/Grove-I2C_Color_Sensor/)

## Einfaches Beispiel

Das erste Beispiel mit dem Farbsensor ist sehr einfach. Die Farbwerte werden eingelesen, 
so skaliert dass die Werte zwischen 0 und 255 liegen und dann wird die RGB LED auf diese Farbwerte gesetzt.


![Erster Versuch](MakeCode_einfach/Farbsensor_einfach_schleife.png)

Die Erweiterung für den  [Farbsensor](https://github.com/calliope-edu/pxt-TCS34725FN) wird benutzt, um die Sensorwerte einzulesen.

![Sensorwerte lesen](MakeCode_einfach/Farbsensor_einfach_einlesen.png)

Für die Skalierung wird als erstes der Maximalwert aller drei Farbkomponenten berechnet, dann werden alle drei Komponenten so skaliert, dass die Werte zwischen 0 und 255 liegen. 

![Sensorwerte lesen](MakeCode_einfach/Farbsensor_einfach_skalieren.png)

Das Ergebnis ist nicht sehr überzeugend! Die LED ist fast weiß, der Farbton läßt sich nur mit Mühe erahnen. 

Alle Dateien für das Beispiel liegen auf [Github](https://github.com/KLOC-Karsten/grove-farbsensor-einfach).


## Zweite Variante: den Farbwert berechnen

Die zweite Variante ist deutlich komplizierter. Hier wird der Farbwert (Hue) berechnet und die RGB LED mit Hilfe des Farbwertes angesteuert. 
Helligkeit und Sättigung werden ignoriert, dadurch ist die Farbe auf der LED deutlich intensiver.
Die Berechnung geschieht nach den Formeln auf [Wikipedia](https://en.wikipedia.org/wiki/HSL_and_HSV).


![Farbwert](MakeCode_Farbwert/Farbsensor_farbwert_schleife.png)

Die Rohwerte vom Sensor werden wie im ersten Beispiel eingelesen. 

![Einlesen](MakeCode_Farbwert/Farbsensor_farbwert_einlesen.png)

Dann wird der Farbwert bestimmt. Der Wertebereich des Farbwertes liegt zwischen 0 und 360 (der Winkel für den Farbkreis).  
Sättigung und Helligkeit brauchen wir hier nicht, also berechnen wir sie auch gar nicht. 

![Hue Berechnen](MakeCode_Farbwert/Farbsensor_farbwert_hue.png)

Und jetzt die Rolle rückwärts: aus den Farbwert werden die RGB Werte berechnet. 
Da für Sättigung und Helligkeit jetzt Maximalwerte angenommen werden, ist das Ergebnis deutlich Farbintensiver als im ersten Beispiel

![RGB Berechnen](MakeCode_Farbwert/Farbsensor_farbwert_rgb.png)

Alle Dateien für das Beispiel liegen auf [Github](https://github.com/KLOC-Karsten/grove-farbsensor).

## Intermezzo: eine Erweiterung mit Farbfunktionen
Um die Funktionen zur Berechnung von Farbwert und RGB nicht jedesmal neu Berechnen zu müssen, habe ich eine kleine Erweiterung 
gebastelt.

Die Erweiterung enthält zwei Funktionen:
- "Farbwert" berechnet den Farbwert (Hue) aus RGB Werten.
- "Farbwert zu Farbe" berechnet aus einen Farbwert (Hue, 0..360) eine Farbe, die direkt an RGB LED ausgegeben werden kann.

Das nächste Beispiel benutzt die beiden Funktionen.

![Farbfunktionen](MakeCode_Farbwert_LCD/Farbfunktionen.png)

Der gesamte Code befindet sich auf [Github](https://github.com/KLOC-Karsten/farbfunktionen)

## Dritte Variante: Werte Ausgabe am LCD Display.

Für das folgende Beispiel sollen die Farbwerte nicht nur durch die RGB LED angezeigt werden, sondern auch lesbar 
auf ein LCD Display geschrieben werden. 

Die Dateien sind auf [Github](https://github.com/KLOC-Karsten/farbsensor-mit-ausgabe) zu finden.

### Vorbereitung

Die benötigte Hardware ist:
- Calliope mini
- Der Grove Farbsensor: weitere Informationen gibt es hier: [Calliope Website](https://calliope.cc/calliope-mini/erweiterungen/sensoren/tcs3414-farbsensor)
- Ein [Grove I2C Hub](https://www.seeedstudio.com/Grove-I2C-Hub-6-Port-p-4349.html)
- Ein [Grove 16x2 Display](https://www.seeedstudio.com/Grove-16-x-2-LCD-Black-on-Red.html)

Folgende MakeCode Erweiterungen werden benötigt:
- [Farbsensor Erweiterung](https://github.com/calliope-edu/pxt-TCS34725FN)
- [Display Erweiterung](https://github.com/calliope-edu/pxt-display)
- Die Erweiterung mit den [Farbfunktionen](https://github.com/KLOC-Karsten/farbfunktionen)


Die Programmierung erfolgt durch MakeCode.

### Aufbau

Der Aufbau ist immer noch relativ einfach!

- Der I2C Hub wird am Anschluss A0 vom Calliope mini angeschlossen.
- Der Farbsensor wird am Hub angeschlossen.
- Das Display wird ebenfalls am Hub angeschlossen.

![Farbfunktionen](MakeCode_Farbwert_LCD/Aufbau.jpg)


### Programmierung

Das Programm führt dauerhaft folgende Berechnungen aus:
- Als erstes werden die Werte für Rot, Grün und Blau eingelesen. 
Ich multipliziere den Rot-Wert noch mal mit 1.2, weil ich immer das Gefühl hatte, die Rot-Werte stimmen nicht so richtig. 
Mit dieser Korrektur stimmen mit meinem Sensor die berechneten Farbwerte viel besser. 
Ohne diese Korrektur wurde beispielsweise Violett immer noch blau ausgegen. 
Am besten selbst ausprobieren! 

- Dann wird der Farbwert aus den Komponenten berechnet. Dieser ist ein Wert von 0 bis 360 Grad und gibt die Position auf dem 
Farbkreis an. Wie oben beschrieben wird das gemacht, um eine bessere Farbsättigung zu erzielen. 

- Als nächstes werden die Texte für die erste und die zweite Zeile des Displays berechnet. 

- Als nächstes werden die Textzeilen auf dem Display ausgegeben.

- Anschließend wird die Farbe für die RGB LED berechnet und diese angesteuert.


