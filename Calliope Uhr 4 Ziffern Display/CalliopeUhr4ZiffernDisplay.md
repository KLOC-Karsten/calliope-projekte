# Calliope Uhr

Ein einfaches Uhr Programm für die Calliope Mini. 
Die Uhrzeit wird auf dem 4 Ziffern Display angezeigt,  
durch Button A wird der Stundenzähler erhöht, 
durch Button B wird der Minutenzähler erhöht. 

Informationen zum 4 Ziffern Display gibt es [hier](https://calliope.cc/calliope-mini/erweiterungen/sensoren/4-ziffern-display)

## Konfiguration

![Konfiguration](Bilder/Konfiguration.png "Konfiguration")


## Hauptprogramm

Das Hauptprogramm startet einen Timer und überprúft dann laufend, 
ob Taste A oder B gedrückt worden sind. 
Falls ja, werden die Stunden oder Minuten erhöht (eingestellt), 
falls nein, wird die Uhrzeit aktualisiert. 
Hat sich die Uhrzeit geändert, dann wird die Anzeige auf dem Display aktualisiert. 



![Hauptprogramm](Bilder/Hauptprogramm.png "Hauptprogramm")

## Uhrzeit aktualisieren

Der Wert des internen Zeitgebers wird in Sekunden umgerechnet und die Startzeit wird dazu addiert. 
Dann wird aus den Sekunden die aktuelle Uhrzeit in Stunden, Minuten und Sekunden berechnet. 
Alle 24 Stunden wird der Zeitgeber zurückgesetzt. 

![Aktualisiere Uhrzeit](Bilder/aktualisiereUhrzeit.png "Aktualisiere Uhrzeit")

## Uhrzeit anzeigen

Auf dem Display kann nur eine Zahl angezeigt werden und es werden keine führenden Nullen angezeigt. 
Der Wert wird wie folgt berechnet 

    Wert := (Stunden * 100) + Minuten

Je nach Uhrzeit kann der Wert mal einstellig (zum Beispiel um 00:02), 
zweistellig (zum Beispiel um 00:12), 
dreistellig (zum Beispiel um 01:23) 
oder vierstellig (12:34) sein. 
Je nach Fall wird die Zahl entsprechend weit nach rechts verschoben.

![Zeige Uhrzeit](Bilder/zeigeUhrzeit.png "Zeige Uhrzeit")

## Stunden einstellen

Diese Funktion wird ausgerufen, wenn Button A gedrückt wird. Die Startzeit wird um eine Stunde (3600 Sekunden) erhöht. 
Danach wird die Uhrzeit neu berechnet und ausgegeben. 

![erhoeheStunden](Bilder/erhoeheStunden.png "erhoeheStunden")

## Minuten einstellen

Diese Funktion wird ausgerufen, wenn Button B gedrückt wird. Die Startzeit wird um eine Minute (60 Sekunden) erhöht. 
Danach wird die Uhrzeit neu berechnet und ausgegeben. 


![erhoeheMinuten](Bilder/erhoeheMinuten.png "erhoeheMinuten")


