# Calliope Temperatur Sensoren

Verschiedene kleine Beispiele mit Temperatursensoren

## SCD30 und interner Temperatursensor
Calliope mini hat einen internen Temperatursensor, der allerdings die Temperatur des Microcontrollers angibt, und die ist meistens einige Grad höher als die Umgebungstemperatur. 

Ich benutze hier den SCD30 als externen Temperatursensor. SCD30 ist hauptsächlich ein CO2 Sensor, brauch man nur einen Temperatursensor, dann gibt es deutlich günstigere Alternativen. 

Angeschlossen wird der Sensor am Grove Anschluss A0. 

Das MakeCode Programm befindet sich auf [Github](https://github.com/KLOC-Karsten/temperatur_scd30_intern).

![Temperaturmessung mit SCD30](SCD30Intern/Temp_SCD30_Calliope.png)

## Interner Temperatursensor mit Ausgabe am Grove LCD

Dieses Beispiel benutzt den internen Temperatur Sensor und gibt die aktuelle Temperatur, 
den Minimum Wert für die Temperatur und den Maximum Wert für die Temperatur 
auf das angeschlossene Grove Display aus. 

Für das Beispiel wird die Erweiterung für das LCD Display benötigt: [display](https://github.com/calliope-edu/pxt-display)

![Aufbau](InternLCD/CalliopeTempLCD.png)

Angeschlossen wird das Display an Anschluss A0.
Das MakeCode Programm befindet sich auf [Github](https://github.com/KLOC-Karsten/grove-lcd-temperaturanzeige).

![Programm](InternLCD/Calliope_Temp_LCD.png)


