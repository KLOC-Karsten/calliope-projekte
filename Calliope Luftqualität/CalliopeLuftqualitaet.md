# Calliope Luftqualität

Beispiele mit den Luftqualität-Sensor. 
Informationen zum Sensor gibt es [hier](https://calliope.cc/calliope-mini/erweiterungen/sensoren/air-quality-sensor-1-3)

Der Sensor misst hauptsächlich Rauch und Alkohol, ist also nur sehr begrenzt in der Lage das Raumklima zu überwachen. 

## Konfiguration

![Konfiguration](Bilder/aqsKonfiguration.png "Konfiguration")


## Anzeige-Programm

Ein sehr einfaches Programm, dass nichts weiter tut, als die Sensorwerte auszugeben. 
Kann man benutzen, um festzustellen, welche Wertebereiche für eine gute und welche für eine schlechte Qualität stehen. 

![Anzeige-Programm](Bilder/anzeigeLuftQualitaet.png "Anzeige-Programm")


## Hauptprogramm


![Hauptprogramm](Bilder/Hauptprogramm.png "Hauptprogramm")

Das Hauptprogramm warnt bei zu schlechter Luft. 
Die Grenzwerte fuer schlechte Luft sind reine Erfahrungswerte, und sollten individuell angepasst werden. 
Neben der Warnung durch Smiley und LED gibt es noch zwei kleine Funktionen:

- Grenzwerte neu setzen: durch Druck auf Taste A können die Grenzwerte angepasst werden. Einfach so lange Taste A drücken, bis 
  der Haken angezeigt wird. Am besten bei frischer Luft machen. 
  Da wird in der Endlosschleife
  3 Sekunden lang warten, wird der Tastendruck eventuell nicht sofort erkannt. 

- Aktuellen Wert anzeigen: durch Druck auf Taste B wird der aktuelle Wert angezeigt. So lange Taste B 
  drücken, bis der aktuelle Wert angezeigt wird, dann die Taste wieder loslassen. Da wird in der Endlosschleife
  3 Sekunden lang warten, wird der Tastendruck eventuell nicht sofort erkannt. 


